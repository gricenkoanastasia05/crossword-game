
const title = document.getElementById('cross-title');
title.style.fontSize = '25px';
title.style.fontFamily = 'Segoe Script';
title.style.color = 'blueviolet';
title.style.textAlign = 'center';
title.style.backgroundColor = 'lightgray';

const crossTable = document.getElementById('cross-table');
crossTable.style.textAlign = 'center';

// $(document).ready(function () {
//   $.get('https://tetratest.mooo.com/?number=10', function(data) {
//       console.log("###", data);
//       $.get(`https://crossword.worddict.net/?words=${data}`), function (data) {
//           run(data);
//       }
//   })
//
// })

const json = '{"width":20,"height":20,"horizontals":\
[{"w":"ОБОЗ","x":8,"y":9},{"w":"АМБАР","x":4,"y":10},{"w":"ГРАММ","x":2,"y":13},{"w":"ЛАДАН","x":10,"y":11},\
{"w":"НАБОР","x":0,"y":11},{"w":"ОЗНОБ","x":13,"y":8},{"w":"РЕДИС","x":14,"y":13},{"w":"СОВЕТ","x":15,"y":6},\
{"w":"АЛТАРЬ","x":11,"y":15},{"w":"АТЕИСТ","x":2,"y":6},{"w":"ЖЕЛЕЗО","x":11,"y":4},{"w":"ЗАПРЕТ","x":4,"y":2},\
{"w":"МНЕНИЕ","x":1,"y":16},{"w":"ОСАДКИ","x":4,"y":18},{"w":"ПОКРОВ","x":1,"y":4},{"w":"РОМАНС","x":8,"y":0},\
{"w":"АГРОНОМ","x":11,"y":17},{"w":"ВТОРНИК","x":13,"y":2}],"verticals":[{"w":"ОРЕЛ","x":8,"y":9},\
{"w":"ЯЗВА","x":11,"y":8},{"w":"АРКАН","x":4,"y":10},{"w":"МОЛВА","x":13,"y":7},{"w":"НОРМА","x":14,"y":11},\
{"w":"ОЛОВО","x":16,"y":4},{"w":"СКАРБ","x":6,"y":6},{"w":"ТАПИР","x":17,"y":10},{"w":"АРЕНДА","x":0,"y":8},\
{"w":"ГАЛОШИ","x":5,"y":1},{"w":"ЖИЛИЩЕ","x":18,"y":1},{"w":"МАНЕРА","x":6,"y":13},{"w":"НАЧАЛО","x":11,"y":14},\
{"w":"ОСМОТР","x":1,"y":14},{"w":"РВЕНИЕ","x":8,"y":0},{"w":"ТЕЛЕГА","x":19,"y":6},{"w":"АКРОБАТ","x":2,"y":1},\
{"w":"КЛАССИК","x":9,"y":13}]}';

// const crossData = JSON.parse(json);

function emptyArray(colCount, rowCount) {
    const table = [];
    for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
        const row = table[rowIndex] = [];
        for (let colIndex = 0; colIndex < colCount; colIndex++) {
            row.push({});
        }
    }
    return table;
}
// const emptyTable = emptyArray(crossData.width, crossData.height);

function drawWord(crossData, table, direction, word, startRow, startColumn) {
    for (let i = 0; i < word.length; i++) {
        if (direction === crossData.horizontals) {
                table[startRow][i + startColumn] = {indexCh: [i, ''], ch: word[i], word: [word, ''], direction: 'HORIZONTAL'};
        }

        if (direction === crossData.verticals) {
            const value = table[startRow + i][startColumn];
            if ( value && value.word) {
                const horWord = value.word[0];
                table[startRow + i][startColumn] = {indexCh: [value.indexCh[0], i],
                    ch: word[i], word: [horWord, word], direction: 'HORIZONTAL/VERTICAL'};
            }
            else {
                table[startRow + i][startColumn] = { indexCh: ['', i], ch: word[i], word: ['', word], direction: 'VERTICAL'};
            }
        }
    }
    return table;
}


function fillAllHorizontalWords(crossData, table, wordsObject ){
    for (let wordIndex = 0; wordIndex < wordsObject.length; wordIndex++) {
        const word = wordsObject[wordIndex];
        drawWord(crossData, table, crossData.horizontals, word.w, word.y, word.x);
    }
    return table;
}
// const allHorizontals = fillAllHorizontalWords(emptyTable, crossData.horizontals);


function fillAllVerticalWords(crossData, table, wordsObject ){
    for (let wordIndex = 0; wordIndex < wordsObject.length; wordIndex++) {
        const word = wordsObject[wordIndex];
        drawWord(crossData, table, crossData.verticals, word.w, word.y, word.x);
    }
    return table;
}
// const allCrosswordTable = fillAllVerticalWords(allHorizontals, crossData.verticals);
// console.log(allCrosswordTable);
// const table = document.getElementById('cross-table');


function showTable(table, crossArray, tableHeight, tableWidth) {
    for (let i = 0; i < tableHeight; i++) {
        const tr = document.createElement('tr');
        for (let j = 0; j < tableWidth; j++) {
            const td = document.createElement('td');
            // cellValue = crossArray[i][j].ch || ' ';
            let cellValue;
            if(crossArray[i][j].ch) {
                cellValue = '';
            }
            else {cellValue = ' '}
            td.innerHTML = cellValue;
            td.id = 'withoutCharacter';
            if (cellValue !== ' ') {
                td.style.backgroundColor = 'pink';
                td.id = `${20*i + j}`;
            }
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

// showTable(allCrosswordTable, crossData.height, crossData.width);

function pushMessage(startTime, enteredWord, wordRight, arrState) {
    const end = Date.now();
    const spentTime = (end - startTime) / 1000;
    const ourMessage = {
        word: enteredWord,
        rightWord: wordRight,
        secondSpent: spentTime,
        whatTime: Date.now().toLocaleString('en-US')
    };
    arrState.push(ourMessage);
    return arrState;
}

function wordColorFill(charId, enteredWord, wordIndex, color) {
    const charOfWord = document.getElementById(charId);
    charOfWord.innerHTML = enteredWord[wordIndex];
    charOfWord.style.backgroundColor = color;
    return charOfWord;
}

function checkHorWord(currentState, startTime, enteredWord, wordRight, wordBeginId, colorRight, colorWrong) {
    pushMessage(startTime, enteredWord, wordRight, currentState);
    for (let j = 0; j < wordRight.length; j++) {
        if (enteredWord === wordRight) {
            wordColorFill(wordBeginId + j, enteredWord, j, colorRight);
        }
        else {
            wordColorFill(wordBeginId + j, enteredWord, j, colorWrong);
        }
    }
    return currentState;
}

function checkVertWord(currentState, startTime, enteredWord, wordRight, wordBeginId, colorRight, colorWrong) {
    pushMessage(startTime, enteredWord, wordRight, currentState);
    for (let j = 0; j < wordRight.length; j++) {
        if (enteredWord === wordRight) {
            wordColorFill(wordBeginId + (20 * j), enteredWord, j, colorRight);
        }
        else {
            wordColorFill(wordBeginId + (20 * j), enteredWord, j, colorWrong);
        }
    }
    return currentState;
}

function cancelInput() {
    const inputWord = document.getElementById('answer');
    inputWord.value = '';
    return inputWord;
}

function cancelAllWords() {
    const inputWords = document.getElementById('inputYourWords');
    inputWords.value = '';
    return inputWords;
}

function submitInput() {
    const inputWord = document.getElementById('answer');
    return inputWord.value;
}




const results = document.getElementById('results');
results.style.fontFamily = 'Segoe Script';
results.style.fontSize = '15px';
const table = document.getElementById('cross-table');
const answer = document.getElementById('answer');
const submitWord = document.getElementById('submit');
const getWrongWords = document.getElementById('wrongWords');
const getRightWords = document.getElementById('rightWords');
const resetTable = document.getElementById('reset');
const checkBoxDirection = document.getElementById('checkbox');

let target;
let currentRow;
let currentCol;
let wordBeginIndex;
let cellValue;
let wordHorBeginId;
let wordVertBeginId;
let startTime;


function run(json) {
    const crossData = JSON.parse(json);
    const emptyTable = emptyArray(crossData.width, crossData.height);
    const allHorizontals = fillAllHorizontalWords(crossData, emptyTable, crossData.horizontals);
    const allCrosswordTable = fillAllVerticalWords(crossData, allHorizontals, crossData.verticals);
    console.log(allCrosswordTable);
    showTable(table, allCrosswordTable, crossData.height, crossData.width);
    const currentState = [];
    table.onclick = function(event) {
        target = event.target;

        currentRow = Math.floor(target.id / 20);
        currentCol = target.id - currentRow * 20;

        cellValue = allCrosswordTable[currentRow][currentCol];


        // const cellWord = cellValue.word;
        // const cellWordHor = cellWord[0];
        // const cellWordVert = cellWord[1];

        wordBeginIndex = cellValue.indexCh;
        wordHorBeginId = currentRow * 20 + currentCol - wordBeginIndex[0];
        wordVertBeginId = currentRow * 20 + currentCol - (20 * wordBeginIndex[1]);
        startTime = Date.now();

        target.style.backgroundColor = 'darkgrey';
    }

    checkBoxDirection.onclick = function (event) {
        if (cellValue.direction === 'HORIZONTAL/VERTICAL') {
            checkBoxDirection.classList.toggle('false');

        }
    }

    submitWord.onclick = function (event) {
        const cellWord = cellValue.word;
        const cellWordHor = cellWord[0];
        const cellWordVert = cellWord[1];
        const wordBeginIndex = cellValue.indexCh;
        // currentState.push(cellValue);

        const wordAnswer = answer.value.toUpperCase();
        currentState.push(wordAnswer);
        if (cellValue.direction === 'HORIZONTAL') {
            checkHorWord(currentState, startTime, wordAnswer, cellWordHor, wordHorBeginId, 'violet', 'red');
        }
        if (cellValue.direction === 'VERTICAL') {
            checkVertWord(currentState, startTime, wordAnswer, cellWordVert, wordVertBeginId, 'pink', 'red');
        }
        if(cellValue.direction === 'HORIZONTAL/VERTICAL') {
            if (checkBoxDirection.className === 'true') {
                checkHorWord(currentState, startTime, wordAnswer, cellWordHor, wordHorBeginId, 'violet', 'red');
            }
            else {
                checkVertWord(currentState, startTime, wordAnswer, cellWordVert, wordVertBeginId, 'pink', 'red');
            }
        }

    }
    getWrongWords.onclick = function (event) {
        results.innerHTML = 'Your result:  ' + JSON.stringify(currentState.filter(a => a.word !== a.rightWord));
        return results;
    }

    getRightWords.onclick = function (event) {
        results.innerHTML = 'Your result:  ' + JSON.stringify(currentState.filter(a => a.word === a.rightWord));
        return results;
    }

    resetTable.onclick = function (event) {
        table.innerHTML = '';
        showTable(table, allCrosswordTable, crossData.height, crossData.width);
    }

}

run(json, JSON.parse(json));

// const ourUrl = 'https://crossword.worddict.net/?words=just,example,how,crossword,can,be,composed';


const newCrosswordSync = document.getElementById('createNewCrossSync');
const newCrosswordAsync = document.getElementById('createNewCrossAsync');




//synchronous request
function crosswordRequestSync(url) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    try {
        xhr.send();
        return xhr.response;
    }
    catch (error) {
        xhr.onerror = function () {
            console.log('Request failed!');
        }
    }

}

newCrosswordSync.onclick = function (event) {
    table.innerHTML = '';
    const ourUrl = 'https://tetratest.mooo.com/?number=10';
    const resultUrl = `https://crossword.worddict.net/?words=${crosswordRequestSync(ourUrl)}`;
    run(crosswordRequestSync(resultUrl));
}

// asynchronous request
// function getNewWordsAsync(url) {
//     const xhr = new XMLHttpRequest();
//     xhr.open('GET', url, true);
//     let crossJsonData;
//     xhr.onload = function() {
//         crossJsonData = xhr.response;
//         console.log('New crossword: [' + xhr.response + ']');
//         run(crossJsonData);
//     }
//     xhr.onerror = function() {
//         console.log('Request failed!');
//     }
//     xhr.send();
// }

function crosswordRequestAsync(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = function() {
        console.log('New crossword: [', xhr.response, ']');
        callback(xhr.response);
    }
    xhr.onerror = function() {
        console.log('Request failed!');
    }
    xhr.send();
}

newCrosswordAsync.onclick = function (event) {
    table.innerHTML = '';
    crosswordRequestAsync('https://tetratest.mooo.com/?number=10', function(newWords) {
    crosswordRequestAsync(`https://crossword.worddict.net/?words=${newWords}`, function(newJson) {
        run(newJson);
    });
    });
}

const sendNewWords = document.getElementById('sendAllWords');
const imputedNewWords = document.getElementById('inputYourWords');

sendNewWords.onclick = function(event) {
    table.innerHTML = '';
    // crosswordRequestAsync('https://tetratest.mooo.com/?number=10', function(newWords) {
    //     console.log('New words: "', newWords, '"');
    console.log('################', imputedNewWords);
        crosswordRequestAsync(`https://crossword.worddict.net/?words=${imputedNewWords}`, function(newJson) {
            run(newJson);
        });
    // });
}

