function newWordsAsync(url) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    let crossNewJsonData;
    xhr.onload = function() {
        crossNewJsonData = xhr.response;
        console.log('New crossword: [' + xhr.response + ']');
    }
    xhr.onerror = function() {
        console.log('Request failed!');
    }
    xhr.send();
}

function crossRequestAsync(url) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = function() {
        const crossJsonData = xhr.response;
        console.log('New words: "' + xhr.response + '"');
        newWordsAsync(`https://crossword.worddict.net/?words=${crossJsonData}`);
    }
    xhr.onerror = function() {
        console.log('Request failed!');
    }
    xhr.send();
}
crossRequestAsync('https://tetratest.mooo.com/?number=10');
